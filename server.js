const express = require("express");
const app = express();
const cors = require("cors");
var bodyParser = require("body-parser");
const jwt = require("express-jwt");
const jwks = require("jwks-rsa");
const dao = require("./db");

const port = process.env.PORT || 8080;

// Middleware
const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 25,
    jwksUri: "https://beleidy.eu.auth0.com/.well-known/jwks.json"
  }),
  audience: "pocket0",
  issuer: "https://beleidy.eu.auth0.com/",
  algorithms: ["RS256"]
});

const authenticationStack = [jwtCheck];

// allow CORS for our react app running locally
const corsOptions = {
  origin: "http://localhost:3000",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
const corsCheck = cors(corsOptions);
app.use(corsCheck);

app.use(bodyParser.json());

// Public Routes
app.get("/", (req, res, next) => {
  dao
    .readPublic()
    .then(rows => {
      res.json({ result: rows });
    })
    .catch(next);
});

app.put("/", (req, res) => {
  const todoInfo = req.body;
  dao.updatePublic(todoInfo);
  res.sendStatus(200);
});

app.delete("/", (req, res) => {
  const todoInfo = req.body;
  dao.dltPublic(todoInfo);
  res.sendStatus(200);
});

// Private routes
app.options("/private"); // for CORS pre-flight requests
app.get("/private", authenticationStack, (req, res, next) => {
  const owner = req.user.sub;
  dao
    .readPriv({ owner })
    .then(rows => {
      res.json({ result: rows });
    })
    .catch(next);
});

app.post("/private", authenticationStack, (req, res) => {
  const owner = req.user.sub;
  const todoInfo = req.body;
  dao.createPriv({ owner, ...todoInfo });
  res.sendStatus(200);
});

app.put("/private", authenticationStack, (req, res) => {
  const owner = req.user.sub;
  const todoInfo = req.body;
  dao.updatePriv({ owner, ...todoInfo });
  res.sendStatus(200);
});

app.delete("/private", authenticationStack, (req, res) => {
  const owner = req.user.sub;
  const todoInfo = req.body;
  dao.dltPriv({ owner, ...todoInfo });
  res.sendStatus(200);
});

const init = () => console.log(`Listening on port ${port}`);
app.listen(port, init);
