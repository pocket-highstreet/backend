const sqlite3 = require("sqlite3").verbose();

class DAO {
  constructor() {
    this.db = new sqlite3.Database("todos.db");

    this.db.run(`CREATE TABLE IF NOT EXISTS todos(
        id  INTEGER PRIMARY KEY,
        note  TEXT,
        owner TEXT,
        done  INT,
        priv INT)`);
  }

  // Public Operations
  readPublic() {
    return new Promise((resolve, reject) => {
      this.db.all(`SELECT * FROM todos WHERE priv=0`, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  updatePublic({ id, done, priv }) {
    this.db.run(`UPDATE todos SET done=$done, priv=$priv WHERE id=$id`, {
      $id: id,
      $done: done,
      $priv: priv
    });
  }

  dltPublic({ id }) {
    this.db.run(`DELETE FROM todos WHERE id=$id`, {
      $id: id
    });
  }

  // Private operations
  readPriv({ owner }) {
    return new Promise((resolve, reject) => {
      this.db.all(
        `SELECT * FROM todos WHERE owner=$owner AND priv=1`,
        { $owner: owner },
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
  }

  createPriv({ note, owner, done, priv }) {
    this.db.run(
      `INSERT INTO todos(note, owner, done, priv) VALUES($note, $owner, $done, $priv)`,
      {
        $note: note,
        $owner: owner,
        $done: done,
        $priv: priv
      }
    );
  }

  updatePriv({ id, done, priv, owner }) {
    this.db.run(
      `UPDATE todos SET done=$done, priv=$priv WHERE id=$id AND owner=$owner`,
      {
        $id: id,
        $owner: owner,
        $done: done,
        $priv: priv
      }
    );
  }

  dltPriv({ id, owner }) {
    this.db.run(`DELETE FROM todos WHERE id=$id AND owner=$owner`, {
      $id: id,
      $owner: owner
    });
  }
}

const dao = new DAO();

module.exports = dao;
