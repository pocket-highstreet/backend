# Pocket Highstreet Backend

## Installing
```
git clone https://gitlab.com/pocket-highstreet/backend.git
cd backend
npm install
node server.js
```
Now the server will be running on `http://localhost:8080`

## Check that it is running correctly
```
curl localhost:8080
```

You should receive a JSON response with all the public todos.